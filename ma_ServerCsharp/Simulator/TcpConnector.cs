﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.IO;
using System.Net;
using System.Net.Sockets;



namespace Simulator
{
    class TcpConnector
    {
        static public Fifo<string> inbuffer;
		static public Fifo<string> outbuffer;
		static private IPAddress serverIp;
		static public int TcpPort = 4711;
		static public bool run = false;
		
		static public Thread th;
		
		static public void Start(IPAddress ipa)
		{
			
			if (run)
				return;
			inbuffer = new Fifo<string>(8);
			outbuffer = new Fifo<string>(8);
			
			serverIp = ipa;
			th = new Thread(new ThreadStart(Run));
			th.Start();
		}
		
		static public void Stop()
		{
			run = false;
		}
		
		static private void Run()
		{  
			run = true;
			NetworkStream stream = null;
			TcpClient c = null;
			try
			{
				// Verbindung zum Server aufbauen
				c = new TcpClient(serverIp.ToString(), TcpPort);

				// Stream zum lesen holen
				stream = c.GetStream();
			}
			catch
			{
				string str = "Verbindungs Aufbau Fehlgeschlagen";
				inbuffer.Put(ref str);
				return;
			}
			
			while (run)
			{
				try
				{
					if (stream.DataAvailable)
					{
						string str = TcpReaderWriter.Readline(stream);
						inbuffer.Put(ref str);
					}
					else
					{
						string str = "";
						if (outbuffer.Get(ref str))
						{
							TcpReaderWriter.WriteLine(str, stream);
						}
					}
					
				}
				catch (Exception)
				{
					// Setze das Schleifen-Flag zurück
					// wenn ein Fehler in der Kommunikation aufgetreten ist
					run = false;
				}
			}
			// Schließe die Verbindung zum Server
			c.Close();
		}
    }
}
