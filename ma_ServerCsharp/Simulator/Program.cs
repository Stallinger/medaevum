﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Net;
namespace Simulator
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpConnector.Start(IPAddress.Parse("192.168.0.3"));
            while(true)
            {
                if (Console.KeyAvailable)
                {
                    string input = Console.ReadLine();
                    TcpConnector.outbuffer.Put(ref input);
                }
                string server = "";
                if(TcpConnector.inbuffer.Get(ref server))
                {
                    Console.WriteLine(server);
                }
            }
        }
    }
}
