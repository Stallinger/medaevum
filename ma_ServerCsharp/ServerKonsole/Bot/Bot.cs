﻿#define DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

namespace ServerKonsole.Bot
{
    partial class Bot
    {
        #region Settings
        public int AreaID{get;set;}
        public string Type{get;set;}
        public PointF Position { get; set; }
        public double Angle { get; set; }
        public float Velocity { get; set; }
        public int Health { get; set; }
        public int Level { get; set; }
        public int CoolDown_Max { get { return 500; } }
        public int CoolDown { get; set; }
        public Area myArea { get { return Area._Areas[AreaID]; } }
        public float Angle360
        {
            get { return (float)Angle * (float)(180 / Math.PI); }
        }

        private int _duration;
        private int _art;
        private float _absmin_alt;
        #endregion

        #region Constructor
        public Bot(string type,int areaId)
        {
            AreaID = areaId;
            Type = type;
            Position = new PointF();
            Health = 100;
            Level = 1;
            CoolDown = 0;
            Velocity = 0.03f;
            _duration = 0;
        }
        #endregion

        #region Dynamic
        /// <summary>
        ///  Setzt den Bot auf einem zufälligen Punkt mit zufälligem Winkel im zugewiesenen Bot Area
        /// </summary>
        public void Spawn()
        {
            Health = 100;
            PointF[] AreaPoints = myArea.Vectors.ToArray();
            float posX = RandomFloat(GetMin(AreaPoints,true),GetMax(AreaPoints,true));
            float posY = 0;

            List<float> senkrechteTreffer = new List<float>();
            for(int i = 0; i < myArea.Walls.Count;i++)
            {
                if(((myArea.Walls[i].PointA.X < posX)&&(myArea.Walls[i].PointB.X > posX))
                    ||(myArea.Walls[i].PointA.X > posX)&&(myArea.Walls[i].PointB.X < posX))
                {
                    senkrechteTreffer.Add(myArea.Walls[i].Get(posX));
                }
            }
            if (senkrechteTreffer.Count != 2)
                Console.WriteLine("Areaberechnung in Area " + AreaID + " nicht möglich");
            else
                posY = RandomFloat(senkrechteTreffer[0], senkrechteTreffer[1]);

            Angle = RandomFloat(0.0f, 1.99f) * Math.PI;
            Position = new PointF(posX, posY);
        }
        /// <summary>
        /// Aktualisiert den Bot wenn er am Leben ist und Uptdate DeltaZeit wird in BotClock festgelegt
        /// </summary>
        public void Step()
        {

            float absmin = float.MaxValue;
            // Gerade in der der Bot gerade sieht
            Storages.LinearFunc lf = new Storages.LinearFunc(Position, (float)Math.Tan(Angle));
            for (int i = 0; i < myArea.Walls.Count;i++ )
            {
                PointF p = Storages.LinearFunc.Intersection(myArea.Walls[i], lf);
                float abs = (float)Math.Sqrt(((p.X - Position.X)*(p.X - Position.X)) + ((p.Y - Position.Y)*(p.Y - Position.Y)));// der abstand zwischen den Schnittpunkt und der Position
                if(absmin > abs)
                {
                    absmin = abs;
                }
            }
            if ((absmin < 1)&&(_absmin_alt > absmin))
            {
                Angle += Math.PI;
                Angle = Angle % (Math.PI * 2);
                _art = 0;
            }
         
            if (_duration == 0)
            {
                if ((_art == 1) || (_art == 2))
                    _art = 0;
                else
                    _art = myRandom.Next(0, 4);
                _duration = myRandom.Next(30, 300);
            }
            _duration--;
            switch (_art)
            {
                case 0:
                    Position = new PointF(Position.X + ((float)Math.Sin(Angle) * Velocity),
                                    Position.Y + ((float)Math.Cos(Angle) * Velocity));
                    break;
                case 1:
                    Angle += 0.03;
                    break;
                case 2:
                    Angle -= 0.03;
                    break;
                case 3:
                    break;
            }

            _absmin_alt = absmin;
        }

        /// <summary>
        /// Aufgerufen von WorkerCommands
        /// </summary>
        /// <param name="dmg"></param>
        public void Hit(int dmg)
        {
            Health = Health - dmg;
            if(Health < 0)
            {
                CoolDown = CoolDown_Max;
            }
        }


        #endregion

        #region Static
        //////////////////////////////////////////////////
        //Statische Methoden
        ////////////////////////////////////////////////////////////

        protected static Random myRandom = new Random();
        public static List<Bot> _Bots = new List<Bot>();
        public static string Path { get { return InOutPut.Daten.PathBotsFile + "bots" + ".cfg"; } }
        public static void Load()
        {
            if (!Directory.Exists(InOutPut.Daten.PathBotsFile))
                Directory.CreateDirectory(InOutPut.Daten.PathBotsFile);

            InOutPut.Textfile tf = new InOutPut.Textfile();
            string[] bottexts = tf.ReadFile(Path).Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in bottexts)
            {
                string[] arguments = s.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                Bot b = new Bot(arguments[0], Convert.ToInt32(arguments[1]));
                _Bots.Add(b);
            }
        }
        #endregion
    }
}
