﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ServerKonsole.Bot
{
    partial class Bot
    {
        #region FromThread
        /// <summary>
        ///  Berechnet alle lebendigen Bots
        /// </summary>
        public static void Calc()
        {
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            for (int i = 0; i < Bot._Bots.Count;i++ )
            {
                //Wenn ein bot lebt hat er Cooldown -1 wenn er stirbt setzt man den cooldown auf zb 500 bei 0 spawnd er wieder und wird auf -1 gesetzt

                if (Bot._Bots[i].CoolDown == 0)
                {
                    Bot._Bots[i].Spawn();
                    Bot._Bots[i].CoolDown = -1;
                }
                else if (Bot._Bots[i].CoolDown == -1)
                {
                    //bot macht einen schritt
                    Bot._Bots[i].Step();
                    
                }
                else
                {
                    Bot._Bots[i].CoolDown--; 
                    if (Bot._Bots[i].CoolDown + 1 != Bot._Bots[i].CoolDown_Max)
                        continue;
        
                }
                //absenden über WorkerCommands SendEachOther
                string msg = "~bot~" + i + "~" + Bot._Bots[i].Type + "~" + Bot._Bots[i].Position.X.ToString(nfi) + "~" + Bot._Bots[i].Position.Y.ToString(nfi) + "~" + Bot._Bots[i].Angle360.ToString(nfi) + "~" + Bot._Bots[i].Health + "~";
                Commands.WorkerCommands.AddToEachOther(-1, msg);
            }
        }

        #endregion

        #region CalcHelps
        public static float GetMax(PointF[] points, bool InX)
        {
            float max;
            if (InX)
            {
                max = points[0].X;
                for (int i = 0; i < points.Length; i++)
                {
                    if (max < points[i].X)
                        max = points[i].X;
                }
            }
            else
            {
                max = points[0].Y;
                for (int i = 0; i < points.Length; i++)
                {
                    if (max < points[i].Y)
                        max = points[i].Y;
                }
            }
            return max;
        }
        public static float GetMin(PointF[] points, bool InX)
        {
            float min;
            if (InX)
            {
                min = points[0].X;
                for (int i = 0; i < points.Length; i++)
                {
                    if (min > points[i].X)
                        min = points[i].X;
                }
            }
            else
            {
                min = points[0].Y;
                for (int i = 0; i < points.Length; i++)
                {
                    if (min > points[i].Y)
                        min = points[i].Y;
                }
            }
            return min;
        }

        /// <summary>
        /// Beide Parameter inclusiv
        /// </summary>
        /// <param name="Min"></param>
        /// <param name="Max"></param>
        /// <returns>Eine Zufallszahl</returns>
        public static float RandomFloat(float Min,float Max)
        {
            return (float)(myRandom.NextDouble() * (Max - Min)) + Min;
        }
        #endregion

    }
}
