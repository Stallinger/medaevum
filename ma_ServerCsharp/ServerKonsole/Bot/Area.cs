﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

namespace ServerKonsole.Bot
{
    class Area
    {
        #region Dynamic
        public List<PointF> Vectors{get;set;}
        public List<Storages.LinearFunc> Walls { get; set; }

        public string Name { get; set; }

        public void ReCalcWalls()
        {
            Walls = new List<Storages.LinearFunc>();
            int j = 0;
            for (int i = 0; i < Vectors.Count; i++)
            {
                j++;
                j = j % Vectors.Count;
                Walls.Add(new Storages.LinearFunc(Vectors[i], Vectors[j]));
            }
        }
        public Area(string name)
        {
            Name = name;
            Vectors = new List<PointF>();
        }
        #endregion

        #region Static
        public static List<Area> _Areas = new List<Area>();

        public static string Path { get { return InOutPut.Daten.PathBotsFile + "areas" + ".cfg"; } }

        public static void Load()
        {
            if(!Directory.Exists(InOutPut.Daten.PathBotsFile))
                Directory.CreateDirectory(InOutPut.Daten.PathBotsFile);

            InOutPut.Textfile tf = new InOutPut.Textfile();
            string [] areatexts = tf.ReadFile(Path).Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach(string s in areatexts)
            {
                string [] arguments = s.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                Area a = new Area(arguments[0]);
                int x = 1;
                while (x < arguments.Length)
                {
                    a.Vectors.Add(new PointF(
                        (float)Convert.ToDouble(arguments[x]),
                        (float)Convert.ToDouble(arguments[x + 1])));
                    x = x + 2;
                }
                a.ReCalcWalls();
                _Areas.Add(a);
            }
        }
        #endregion
    }
}
