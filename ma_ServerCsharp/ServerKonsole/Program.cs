﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.IO;
using System.Net.Sockets;
using System.Reflection;
using System.Diagnostics;

namespace ServerKonsole
{
    class Program
    {
        public static UdpClient _broadcaster;
        public static DateTime _servertime;
        public static TcpListener _listener;

        public static List<Threads.abstractThread> _threads;
        public static Storages.Fifo<Storages.Message> _traffic;

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            
            if (AlreadyRunning())
            {
                Console.Write("Server already running...");
                Console.Read();
                return;
            }
            
            if(!InOutPut.Daten.LoadServerFile())
            {
                Console.Write("Set ServerName: ");
                InOutPut.Daten.SetConfWert("servername", Console.ReadLine());
            }
            Console.WriteLine(PrintLogo());

            _threads = new List<Threads.abstractThread>();

            _threads.Add(new Threads.ServerTime());
            _threads.Add(new Threads.UdpBroadcaster());
            _threads.Add(new Threads.ServerController());
            _threads.Add(new Threads.Worker());
            _threads.Add(new Threads.BotClock());

            ConsoleInput();
        }

        public static void ConsoleInput()
        {
            while(true)
            {
                Console.Write(">>");
                Commands.ConsoleCommands.Work(Console.ReadLine());
            }
        }


        private static bool AlreadyRunning()
        {
            Process current = Process.GetCurrentProcess();
            Process[] processes = Process.GetProcessesByName(
                                    current.ProcessName);
            foreach (Process process in processes)
            {
                if (process.Id != current.Id)
                {
                    if (Assembly.GetExecutingAssembly().Location
                        .Replace("/", "\\") == current.MainModule.FileName)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        
        public static string PrintLogo()
        {
            string logo = "";
            for (int i = 0; i < 50; i++)
                logo = logo + "#";
            logo = logo + "\r\n";

            logo += "\t\tMEDAEVUM SERVER\r\n\r\n";
            logo += "\tserver-name:\t\t" + InOutPut.Daten.GetConfWert("servername") + "\r\n";
            logo += "\tip-adress:\t\t" + InOutPut.Info.InterneIP() + "\r\n";
            logo += "\tudp broadcast port:\t" + InOutPut.Info.UdpPort.ToString() + "\r\n";
            logo += "\ttcp output port:\t" + InOutPut.Info.TcpPort.ToString() + "\r\n";
            for (int i = 0; i < 50; i++)
                logo = logo + "#";

            return logo;
        }


    }
}
