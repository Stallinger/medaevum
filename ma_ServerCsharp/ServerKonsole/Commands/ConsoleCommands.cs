﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace ServerKonsole.Commands
{

    class ConsoleCommands
    {
        public static void Work(string input)
        {
            switch(input.ToLower())
            {
                case "show online players":
                    for (int i = 0; i < Player.Player.OnlinePlayers.Count; i++)
                    {
                        if (Player.Player.OnlinePlayers.ElementAt(i).Value != null)
                            Console.WriteLine("\t" + i + "\t" + Player.Player.OnlinePlayers.ElementAt(i).Key + "\t" + Player.Player.OnlinePlayers.ElementAt(i).Value.Username); 
                    }
                    break;
                case "show bot areas":
                    for(int i = 0; i < Bot.Area._Areas.Count;i++)
                    {
                        Console.WriteLine("\tid: " + i);
                        Console.WriteLine("\tname: " + Bot.Area._Areas[i].Name);
                        for(int j = 0; j < Bot.Area._Areas[i].Vectors.Count;j++)
                        {
                            Console.WriteLine("\t\tX:" + Bot.Area._Areas[i].Vectors[j].X.ToString("f3") + "\tZ:" + Bot.Area._Areas[i].Vectors[j].Y.ToString("f3"));
                        }
                    }
                    break;
                case "show bot list":
                    for(int i = 0;i < Bot.Bot._Bots.Count;i++)
                    {
                        Console.WriteLine("\ttype: " + Bot.Bot._Bots[i].Type + "\tareaId: " + Bot.Bot._Bots[i].AreaID);
                    }
                    break;
                case "set server name":
                   Console.Write("\tserver-name >>");
                   InOutPut.Daten.SetConfWert("servername", Console.ReadLine());
                   Console.Clear();
                   Console.WriteLine(Program.PrintLogo());
                   break;
                default:
                        Console.WriteLine("\tno command found");
                    break;
            }
        }



        private const string _inputerror = "\twrong input format";
        public static double GetDoubleInput(string text)
        {
            Console.Write(text + " {0.001-99999.999} : ");
            string str = Console.ReadLine();
            double d = 0;
            try
            {
                d = Convert.ToDouble(str);
            }
            catch
            {
                Console.WriteLine(_inputerror);
                GetDoubleInput(text);
            }
            return d;
        }
        public static int GetIntegerInput(string text)
        {
            Console.Write(text + " {0-99999} : ");
            string str = Console.ReadLine();
            int i = 0;
            try
            {
                i = Convert.ToInt32(str);
            }
            catch
            {
                Console.WriteLine(_inputerror);
                GetIntegerInput(text);
            }
            return i;
        }
        public static bool GetBoolInput(string text)
        {
            Console.Write(text + @" {0/1} : ");
            string str = Console.ReadLine();
     
            if (str.Length <= 1)
            {
                if (str[0] == '0')
                    return false;
                else if (str[0] == '1')
                    return true;
                else
                {
                    Console.WriteLine(_inputerror);
                    return GetBoolInput(text);
                }
            }
            Console.WriteLine(_inputerror); 
            return GetBoolInput(text); ;
        }
    }
}
