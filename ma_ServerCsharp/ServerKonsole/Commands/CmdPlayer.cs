﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Threading;

namespace ServerKonsole.Commands
{
    static partial class WorkerCommands
    {
        public static void NewPlayer(string [] s,int threadid)
        {
            if (File.Exists(InOutPut.Daten.PathPlayerFile + s[1] + ".cfg"))
            {
                AddMessage(threadid, "~newplayer~username already exist");
            }
            else
            {
                if (Player.Player.OnlinePlayers.ContainsKey(threadid))
                {
                    Player.Player.OnlinePlayers.Remove(threadid);
                }
                InOutPut.Textfile tf = new InOutPut.Textfile();
                Directory.CreateDirectory(InOutPut.Daten.PathPlayerFile);
                tf.WriteFile(InOutPut.Daten.PathPlayerFile + s[1] + ".cfg", "password=" + s[2]);
                Player.Player p = new Player.Player();
                p.Password = s[2];
                p.Username = s[1];

                Player.Player.OnlinePlayers.Add(threadid,p);
                AddMessage(threadid, "~newplayer~loggedin");
            }

            
        }

        public static void LoadPlayer(string [] s,int threadid)
        {
            if (File.Exists(InOutPut.Daten.PathPlayerFile + s[1] + ".cfg"))
            {
                if(s[2] == InOutPut.Daten.GetPlayerWert(s[1],"password"))
                {
                    if(Player.Player.OnlinePlayers.ContainsKey(threadid))
                    {
                        Player.Player.OnlinePlayers.Remove(threadid);
                    }
                    Player.Player p = new Player.Player();
                    p.Password = s[2];
                    p.Username = s[1];
                    p.Load();
                    Player.Player.OnlinePlayers.Add(threadid,p);
                    AddMessage(threadid, "~loadplayer~loggedin");
                    SendParas(threadid, p);
                }
                else
                {
                    AddMessage(threadid, "~loadplayer~wrong password");
                }
            }
            else
            {
                AddMessage(threadid, "~loadplayer~username not exist");
            }
        }

        public static void Logout(int threadid)
        {
            Player.Player.OnlinePlayers[threadid].Save() ;
            Player.Player.OnlinePlayers[threadid] = null;
  

            Threads.ServerController sc = (Threads.ServerController)Program._threads[2];
            Threads.ServerControls.Client c = sc._clients[threadid];
            c._run = false;
        }

        private static void SendParas(int threadid,Player.Player p)
        {
            Thread.Sleep(200);
            AddMessage(threadid, "~setposition~" + p.Position + "~");
        }
    }
}
