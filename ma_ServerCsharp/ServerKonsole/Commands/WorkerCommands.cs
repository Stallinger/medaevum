﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerKonsole.Commands
{
    static partial class WorkerCommands
    {
        public static void Work(Storages.Message m)
        {
            string[] mparts = m.Text.Split(new string[] { "~" }, StringSplitOptions.RemoveEmptyEntries);
            switch(mparts[0])
            {
                case "newplayer":
                    NewPlayer(mparts, m.ThreadID);
                    break;
                case "loadplayer":
                    LoadPlayer(mparts, m.ThreadID);
                    break;
                case "logout":
                    Logout(m.ThreadID);
                    break;
                case "position":
                    Player.Player.OnlinePlayers[m.ThreadID].Position = mparts[1];
                    AddToEachOther(m.ThreadID, "~player~" +  Player.Player.OnlinePlayers[m.ThreadID].Username + m.Text);
                    break;
                case "bothit":
                    Bot.Bot._Bots[Convert.ToInt32(mparts[1])].Hit(Convert.ToInt32(mparts[2]));
                    break;
                case "c":
                    AddToEachOther(-1, "~c~" + DateTime.Now.ToShortTimeString() + " >> " + Player.Player.OnlinePlayers[m.ThreadID].Username + " >> " + mparts[1] + "~");
                    break;
            }
            

        }

        
        public static void AddMessage(int threadid,string message)
        {
            Threads.ServerController sc = (Threads.ServerController)Program._threads[2];
            Threads.ServerControls.Client c = sc._clients[threadid];
            c.buffer.Put(ref message);
        }

        public static void AddToEachOther(int threadid,string message)
        {
            Threads.ServerController sc = (Threads.ServerController)Program._threads[2];
            for (int i = 0; i < sc._clients.Count;i++ )
            {
                if (i == threadid)
                    continue;

                if (sc._clients[i] != null)
                {
                    if (sc._clients[i]._run)
                    {
                        sc._clients[i].buffer.Put(ref message);
                    }
                }
            }
        }
    }
}
