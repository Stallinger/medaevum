﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;


namespace ServerKonsole.InOutPut
{
    public class Info
    {
        static public int TcpPort { get { return 4711; } }
        static public int UdpPort { get { return 4713; } }
        
        static public string InterneIP()
        {
            string computername = Dns.GetHostName();
            IPHostEntry ipEintrag = Dns.GetHostEntry(computername);
            IPAddress[] IPAdressen = ipEintrag.AddressList;
            foreach (IPAddress ip in IPAdressen)
            {
                if (FindIPFormat(ip.ToString()))
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Ip nicht gefunden");
        }
        static private bool FindIPFormat(string str)
        {
            int anz = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '.')
                {
                    anz++;
                }
            }
            if (anz == 3)
                return true;
            else
                return false;
        }
    }
}
