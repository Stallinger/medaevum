﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Sockets;

namespace ServerKonsole.InOutPut
{
    static public class UdpBroadCaster
    {
      

        static public void Send(UdpClient uc,Byte[] sendBytes)
        {
            try
            {
                uc.Send(sendBytes, sendBytes.Length, new IPEndPoint(IPAddress.Parse("255.255.255.255"), Info.UdpPort));
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
    }
}
