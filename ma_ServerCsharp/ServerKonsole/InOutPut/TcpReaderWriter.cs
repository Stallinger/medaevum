﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;


namespace ServerKonsole.InOutPut
{
    public static class TcpReaderWriter
    {
        public static void  WriteLine(string text,Stream s)
        {
            if ((s == null)||(text == ""))
                throw new ArgumentNullException();
            try
            {
                byte[] buffer = Encoding.UTF8.GetBytes(text + "\r\n");
                s.Write(buffer, 0, buffer.Length);
                s.Flush();
            }
            catch
            {
                throw;
            }
        }

        public static string Readline(Stream s)
        {
            if (s == null)
                throw new ArgumentNullException();
            StreamReader inStream = new StreamReader(s,Encoding.UTF8);
            try
            {
                // Hole nächsten Zeitstring vom Server
                String time = inStream.ReadLine();
                // Setze das Schleifen-Flag zurück
                // wenn der Server aufgehört hat zu senden
                return time;
            }
            catch (Exception exc)
            {
                // Setze das Schleifen-Flag zurück
                // wenn ein Fehler in der Kommunikation aufgetreten ist
                return exc.Message;
            }
          
        }
    }
}
