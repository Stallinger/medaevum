﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace ServerKonsole.InOutPut
{
    public static class Daten
    {
        static public string PathlogFile {get{return Path.Combine(Environment.CurrentDirectory , @"errors.log");}}
        static public string PathServerFile { get { return Path.Combine(Environment.CurrentDirectory, @"data\server.cfg"); } }
        static public string PathPlayerFile { get { return Path.Combine(Environment.CurrentDirectory, @"data\players\"); } }
        static public string PathBotsFile { get { return Path.Combine(Environment.CurrentDirectory, @"data\bots\"); } }

        static public void AddLog(string str)
        {
            str = DateTime.Now.ToString() + " >> " + str;
            Textfile tf = new Textfile();
            if (!File.Exists(PathlogFile))
                tf.WriteFile(PathlogFile, str);
            else
                tf.Append(PathlogFile, "\r\n" + str);
        }

        static private Dictionary<string,string> ServerFileArguments;
        static public bool LoadServerFile()
        {
            ServerFileArguments = new Dictionary<string, string>();
            if (File.Exists(PathServerFile))
            {
                Textfile tf = new Textfile();
                string [] a = tf.ReadFile(PathServerFile).Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in a)
                {
                    string [] x = s.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    ServerFileArguments.Add(x[0],x[1]);
                }
                return true;
            }
            else
            {
                Directory.CreateDirectory(Path.GetDirectoryName(PathServerFile));
                return false;
            }
        }

        static public string GetConfWert(string id)
        {
            return ServerFileArguments[id];
        }
        static public void SetConfWert(string id,string value)
        {
            ServerFileArguments[id] = value;
            
            Textfile tf = new Textfile();
            for (int i = 0; i < ServerFileArguments.Count;i++ )
            {
                if(ServerFileArguments.ElementAt(i).Key == id)
                {
                    tf.WriteLine(PathServerFile,i + 1,id + "=" + value,true);
                }
            }
        }


        static public string GetPlayerWert(string username, string id)
        {
            Textfile tf = new Textfile();
            string str = tf.ReadFile(PathPlayerFile + username + ".cfg");
            try
            {
                string[] a = str.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in a)
                {
                    string[] x = s.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    if (x[0] == id)
                        return x[1];
                }
            }
            catch (Exception)
            {

                return "";
            }
            return "";
        }
       

    }
}
