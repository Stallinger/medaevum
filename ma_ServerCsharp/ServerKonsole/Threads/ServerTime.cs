﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace ServerKonsole.Threads
{
    class ServerTime : abstractThread
    {
        
        public ServerTime()
        {
            _run = true;
            var = 100;
            Program._servertime = new DateTime(800, 1, 1, 12, 0, 0, 0, DateTimeKind.Local);
            Thread th = new Thread(Runner);
            th.Start();
        }

        private byte var;
        protected override void Runner()
        {
            while (_run)
            {
                Program._servertime = Program._servertime.Add(new TimeSpan(0, 0, 1, 0, 0));
                Thread.Sleep(1000);
                if (var >= 20)
                {
                    InOutPut.Daten.SetConfWert("servertime", Program._servertime.ToString());
                    var = 0;
                }
                var++;
            }
        }
    }
}
