﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.IO;
using System.Net.Sockets;

using ServerKonsole.InOutPut;
using ServerKonsole.Threads.ServerControls;

namespace ServerKonsole.Threads
{
    class ServerController : abstractThread
    {
        public List<Client> _clients;
     
        public ServerController()
        {
            _run = true;
            _clients = new List<Client>();
            Program._listener =  new TcpListener(Info.TcpPort);
            Program._listener.Start();
            Program._traffic = new Storages.Fifo<Storages.Message>(30);
            Thread th = new Thread(Runner);
            th.Start();
        }

        protected override void Runner()
        {
            while (_run)
            {

                // Wartet auf eingehenden Verbindungswunsch
                TcpClient c = Program._listener.AcceptTcpClient();
                // Initialisiert und startet einen Server-Thread
                // und fügt ihn zur Liste der Server-Threads hinzu
                _clients.Add(new Client(c, _clients.Count));
            }
        }
    }
}
