﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerKonsole.Threads
{
    abstract class abstractThread
    {

        public bool _run;

        protected abstract void Runner();
    }
}
