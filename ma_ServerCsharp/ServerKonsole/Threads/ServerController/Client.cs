﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.IO;
using System.Net.Sockets;



namespace ServerKonsole.Threads.ServerControls
{
    class Client : abstractThread
    {

        public TcpClient connection = null;
        public int threadid;

        public Client(TcpClient c,int id)
        {
            _run = true;
            threadid = id;
            buffer = new Storages.Fifo<string>(10);
            this.connection = c;
            Thread th = new Thread(Runner);
            th.Start();
        }

       
        public Storages.Fifo<string> buffer;




        protected override void Runner()
        {
            NetworkStream stream = this.connection.GetStream ();
            bool loop = true;
            while ( loop )
            {
              try
              {
                  if (stream.DataAvailable)
                  {
                        string str = InOutPut.TcpReaderWriter.Readline(stream);
                        if (str != "")
                        {
                            Storages.Message m = new Storages.Message(threadid, str);
                            if (!Program._traffic.Put(ref m))
                                Console.WriteLine("\n\t\tSERVER FULL");
                        }
                  }
                  else
                  {
                      string str = "";
                      if(buffer.Get(ref str))
                      {
                          InOutPut.TcpReaderWriter.WriteLine(str, stream);
                      }
                  }
                

                loop = this._run;
              }
              catch ( Exception )
              {
                // oder bis ein Fehler aufgetreten ist
                loop = false;
              }
            }
            // Schließe die Verbindung zum Client
            this.connection.Close ();
            // Setze das Flag "Thread läuft" zurück
            _run = false;
        }

        
    }
}
