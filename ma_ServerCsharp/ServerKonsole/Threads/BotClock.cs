﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ServerKonsole.Threads
{
    class BotClock : abstractThread
    {
 
     
        public BotClock()
        {
            _run = true;
            Bot.Area.Load();
            Bot.Bot.Load();
            Thread th = new Thread(Runner);
            th.Start();
        }

        protected override void Runner()
        {
            while (_run)
            {
                Thread thCalc = new Thread(Bot.Bot.Calc);
                thCalc.Start();
                Thread.Sleep(30);
            }
        }
    }
}


