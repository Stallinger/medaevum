﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.IO;
using System.Net.Sockets;

namespace ServerKonsole.Threads
{
    class UdpBroadcaster : abstractThread
    {

        public UdpBroadcaster()
        {
            _run = true;
            Program._broadcaster = new UdpClient();
            Thread _thUdpBroadcaster = new Thread(Runner);
            _thUdpBroadcaster.Start();
        }

        protected override void Runner()
        {
            while (_run)
            {
                Byte[] ipaddr = Encoding.UTF8.GetBytes(InOutPut.Info.InterneIP() + "~" + InOutPut.Daten.GetConfWert("servername"));
                InOutPut.UdpBroadCaster.Send(Program._broadcaster, ipaddr);
                Thread.Sleep(5000);
            }
        }
    }
}
