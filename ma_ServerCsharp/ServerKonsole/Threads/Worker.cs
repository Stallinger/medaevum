﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ServerKonsole.Threads
{
    class Worker : abstractThread
    {

        public Worker()
        {
            _run = true;
            Thread th = new Thread(Runner);
            th.Start();
        }


        protected override void Runner()
        {
            while(_run)
            {
                Storages.Message m = new Storages.Message();
                if (Program._traffic.Get(ref m))
                {
                    Commands.WorkerCommands.Work(m);
                }
                else
                    Thread.Sleep(1);
            }
        }

    }
}
