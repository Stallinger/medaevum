﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerKonsole.Player
{
    class Player
    {
        public static Dictionary<int,Player> OnlinePlayers = new Dictionary<int,Player>();

        public Player()
        { }

        public string Username { get; set; } //nicht in datei
        public string Password { get; set; } //index 1

        public string Position { get; set; } //index 2

        public string Path { get { return InOutPut.Daten.PathPlayerFile + Username + ".cfg"; } }
        public void Load()
        {
            Position = InOutPut.Daten.GetPlayerWert(Username, "position");
        }

        public void Save()
        {
            InOutPut.Textfile tf = new InOutPut.Textfile();
            tf.WriteLine(Path, 2, "position=" + Position, true);
        }

       ~Player()
       {

       }
    }
}
