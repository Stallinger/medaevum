﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerKonsole.Storages
{
    class Message
    {
        public int ThreadID { get; set; }
        public string Text { get; set; }

        public Message(int id,string msg)
        {
            ThreadID = id;
            Text = msg;
        }

        public Message()
        {

        }
    }
}
