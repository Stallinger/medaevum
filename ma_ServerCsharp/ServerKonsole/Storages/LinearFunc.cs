﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ServerKonsole.Storages
{
    class LinearFunc
    {
        private float _k;
        private float _d;
        public float K { get {return _k;}}
        public float D { get { return _d; } }
        public PointF PointA { get; set; }
        public PointF PointB { get; set; }
        public LinearFunc(PointF p1,PointF p2)
        {
            _k = (p2.Y - p1.Y) / (p2.X - p1.X);
            _d = p1.Y - (_k * p1.X);
            PointA = p1;
            PointB = p2;
        }
        public LinearFunc(PointF pos, float k)
        {
            PointA = pos;
            _k = k;
            _d = pos.Y - (_k * pos.X);
        }

        public LinearFunc(float k, float d)
        {
            PointA = new PointF(0, 0);
            PointB = new PointF(0, 0);
            _k = k;
            _d = d;
        }
        public LinearFunc() : this(1, 0) { }

        public float Get(float x)
        {
            return (_k * x) + _d;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lfA"></param>
        /// <param name="lfB"></param>
        /// <returns>Schnittpunkt</returns>
        public static PointF Intersection(LinearFunc lfA,LinearFunc lfB)
        {
            if (lfB.K - lfA.K == 0)
                return new PointF();

            float x = (lfA.D - lfB.D) / (lfB.K - lfA.K);
            float y = lfA.Get(x);

            return new PointF(x, y);
        }
    }
}
 