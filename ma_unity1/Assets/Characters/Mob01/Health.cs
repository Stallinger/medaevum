﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
	
	public GameObject animationGODie;

	
	public void Die() {
		Vector3 pos = gameObject.transform.position;
		Destroy (gameObject);
		Instantiate (animationGODie,pos , Quaternion.identity);
	}
}
