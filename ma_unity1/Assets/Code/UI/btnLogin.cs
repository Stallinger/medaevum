﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using System.IO;
using System.Net;
using System.Threading;

public class btnLogin : MonoBehaviour {

	public InputField newun;
	public InputField newpw1;
	public InputField newpw2;

	public InputField loadun;
	public InputField loadpw;

	public Text status;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void btnConnect_Click()
	{
		IPAddress ip = UdpReceiver.adress;
		TcpConnection.Start (ip);
		Thread.Sleep(100);
		string str = "~loadplayer~" + loadun.text + "~" + loadpw.text + "~";
		TcpConnection.outbuffer.Put(ref str);
		while(true)
		{
			string erg = "";
			bool b = TcpConnection.inbuffer.Get(ref erg);
			if(b)
			{
				string [] s = erg.Split(new string []{"~"},System.StringSplitOptions.RemoveEmptyEntries);
				if(s[0] == "loadplayer")
				{
					if(s[1] == "loggedin")
						Application.LoadLevel(5);
					else
						status.text = s[1];
					break;
				}
			}
		}
	}

	public void btCreate_Click()
	{
		if((newpw1.text == newpw2.text)&&(newpw1.text != "")&&(newun.text != ""))
		{
			IPAddress ip = UdpReceiver.adress;
			TcpConnection.Start (ip);
			string str = "~newplayer~" + newun.text + "~" + newpw1.text + "~";
			TcpConnection.outbuffer.Put(ref str);
			string erg = "";
			while(true)
			{
				bool b = TcpConnection.inbuffer.Get(ref erg);
				if(b)
				{
					string [] s = erg.Split(new string []{"~"},System.StringSplitOptions.RemoveEmptyEntries);
					if(s[0] == "newplayer")
					{
						if(s[1] == "loggedin")
							Application.LoadLevel(5);
						else
							status.text = s[1];
						break;
					}
				}

			}

		}
	}

}
