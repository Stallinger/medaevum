﻿using UnityEngine;
using System.Collections;

public class MoveSelection : MonoBehaviour {

	public UnityEngine.UI.InputField thisField;
	public UnityEngine.UI.InputField nextField;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
				if ((Input.GetKeyDown (KeyCode.Tab) || Input.GetKeyDown (KeyCode.Return))&&(thisField.isFocused)) {
						thisField.DeactivateInputField ();
						//nextField.Select ();
						nextField.ActivateInputField ();
						
				}
		}
}
