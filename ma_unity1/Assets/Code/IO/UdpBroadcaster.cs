﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Linq;

using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;


/*Stalli 22.01.15 
 * erstellt einen thread der udp broadcasts sendet
 * damit kann der server erkennen es gibt einen neuen client im netzwerk
 * voll funktionsfähig
 */

public class UdpBroadcaster : MonoBehaviour {

	private const int _udpport = 9867; //der hardware zu dem überall im netz gesendet wird
	public bool _threadrun = true; // wenn man hier auf false setzt wird der thread gestoppt

	// Use this for initialization
	void Start () {
		Thread t = new Thread (BroadCasterThread); //internen prozess für udp broadcasts anlegen
		t.Start (); // starten
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void BroadCasterThread()
	{
		UdpClient myUdpClient = new UdpClient ();
		while (_threadrun) 
		{
			byte[] sendBytes = Encoding.UTF8.GetBytes(InterneIP()); // zerlegt die ip adresse in bytes und sendet sie als text weg

			myUdpClient.Send(sendBytes, sendBytes.Length, new IPEndPoint(IPAddress.Parse("255.255.255.255"), _udpport)); 

			Thread.Sleep(10000);// prozess schläft 10sekunden 
		}
	}



	static public string InterneIP() // Interne private ip vom Domain name server holen
	{
		string computername = Dns.GetHostName();
		IPHostEntry ipEintrag = Dns.GetHostEntry(computername);
		IPAddress[] IPAdressen = ipEintrag.AddressList;
		foreach (IPAddress ip in IPAdressen)
		{
			if (FindIPFormat(ip.ToString()))
			{
				return ip.ToString();
			}
		}
		return "";
	}
	static private bool FindIPFormat(string str) // die adress liste durchsuchen um die richtige ip adresse zufinden, damit es nicht ipv6 erwischt
	{
		int anz = 0;
		for (int i = 0; i < str.Length; i++)
		{
			if (str[i] == '.')
			{
				anz++;
			}
		}
		if (anz == 3)
			return true;
		else
			return false;
	}
}
