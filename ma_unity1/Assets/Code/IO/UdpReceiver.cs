﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using UnityEngine.UI;
using UnityEngine.EventSystems;
// stalli funktioniert aber schaut scheiße aus

public class UdpReceiver : MonoBehaviour {
	
	bool _run = true;
	public static IPAddress adress;
	private List<string> _messages;
	private int abgearbeiteteM = 0;
	public RectTransform panel;

	// Use this for initialization
	void Start () {
		Thread thUdpReceiver = new Thread (UdpReceive);
		thUdpReceiver.Start ();
		_messages = new List<string> ();


	}

	void AddMessage(string message)
	{
		bool kommtvor = false;
		for (int i = 0; i < _messages.Count; i++) {
			if(message == _messages[i])
			{
				kommtvor = true;
			}
		}
		if (!kommtvor) {
			_messages.Add (message);
		}
	}
	// Update is called once per frame
	public void Update () {
		if (_messages.Count != abgearbeiteteM) {
			AddButton(_messages[abgearbeiteteM],0,(int)(panel.sizeDelta.y/2f) - 40 - (50 * abgearbeiteteM));
			abgearbeiteteM = _messages.Count;
		}
	}
	

	void UdpReceive()
	{
		UdpClient uc = new UdpClient (4713);
		IPEndPoint ipep = new IPEndPoint (IPAddress.Any,4713);
		while (_run) {

			byte[] bytes = uc.Receive(ref ipep);
			string message = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
			AddMessage(message);
		}
	}



	void AddButton(string Text,int posx,int posy)
	{
		string [] paras = Text.Split (new string [] {"~"}, System.StringSplitOptions.RemoveEmptyEntries);
		GameObject buttonObject = new GameObject("Button");
		Image image = buttonObject.AddComponent<Image>();
		image.transform.parent = this.gameObject.transform;
		image.rectTransform.sizeDelta = new Vector2 (panel.sizeDelta.x - 20, 40);;
		image.rectTransform.anchoredPosition = new Vector2 (posx, posy);
		image.color = Color.grey;
		
		Button button = buttonObject.AddComponent<Button>();
		button.targetGraphic = image;
		button.onClick.AddListener (() => {
						Click (paras[0]);});
		
		GameObject textObject = new GameObject("Text");
		textObject.transform.parent = buttonObject.transform;
		Text text = textObject.AddComponent<Text>();
		text.rectTransform.sizeDelta = Vector2.zero;
		text.rectTransform.anchorMin = Vector2.zero;
		text.rectTransform.anchorMax = Vector2.one;
		text.rectTransform.anchoredPosition = new Vector2(.5f, .5f);
		text.text = "Server-Name: " + paras[1] + "\t\t" + "Ip-Adress: " + paras[0];
		text.font = Resources.FindObjectsOfTypeAll<Font>()[0];
		text.fontSize = 20;
		text.color = Color.white;
		text.alignment = TextAnchor.MiddleCenter;
	}

	void Click(string ip)
	{
		_run = false;
	
		adress = IPAddress.Parse (ip);
		Application.LoadLevel (2);
	}

}
