﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


	public class Fifo<T>
	{
		private int maxSize;
		public int MaxSize
		{
			get { return maxSize; }
		}
		private T [] data;
		private int ir, iw;
		
		public Fifo(int _MaxSize)
		{
			maxSize = _MaxSize;
			data = new T[MaxSize];
			ir = 0;
			iw = 0;
		}
		
		
		
		public bool Put(ref T wert)
		{
			
			if (iw + 1 == ir || ir == 0 && iw + 1 == MaxSize)
				return false; //FIFO voll
			
			data[iw] = wert;
			iw++;
			
			if (iw == MaxSize)
				iw = 0;
			
			return true;
		}
		public bool Get(ref T wert)
		{
			
			if (ir == iw)
				return false; //Fifo leer
			
			wert = data[ir];
			
			ir++;
			
			if (ir == MaxSize)
				ir = 0; 
			
			
			return true;
		}
	}

