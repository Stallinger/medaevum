﻿using UnityEngine;
using System.Collections;

public class InputKeys : MonoBehaviour {

	public UnityEngine.UI.InputField txtChatInput;
	public UnityEngine.UI.InputField txtChatView;
	public UnityEngine.Canvas cavMenue;
	public GameObject InputController;

	// Use this for initialization
	void Start () {
		Debug.Log ("skript wird ausgeführt");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.Tab))
		{
			if(txtChatInput.isFocused)
			{
				txtChatInput.text = "";
				txtChatInput.DeactivateInputField();
				InputController.GetComponent<CharacterMotor>().enabled = true;
			}
			else
			{
				txtChatInput.ActivateInputField();
				txtChatInput.Select();
				InputController.GetComponent<CharacterMotor>().enabled = false;
			}
		}
		if 	((Input.GetKeyDown (KeyCode.Return))&&(txtChatInput.IsActive()))
		{
			if(txtChatInput.text != "")
			{
				InputController.GetComponent<CharacterMotor>().enabled = true;
				string str = "~c~" + txtChatInput.text + "~";
				TcpConnection.outbuffer.Put(ref str);
				txtChatInput.text = "";
			}
		}
		if (txtChatView.isFocused)
			txtChatView.DeactivateInputField ();
		if (Input.GetKeyDown (KeyCode.Escape) && (!this.cavMenue.enabled)) {
			this.cavMenue.enabled = true;
			Screen.showCursor = true;
			InputController.GetComponent<MouseLook>().enabled = false;
			InputController.GetComponent<CharacterMotor>().enabled = false;
		} 
		else if(Input.GetKeyDown (KeyCode.Escape) && (this.cavMenue.enabled))
		{
			this.cavMenue.enabled = false;
			Screen.showCursor = false;
			InputController.GetComponent<MouseLook>().enabled = true;
			InputController.GetComponent<CharacterMotor>().enabled = true;
		

		}
	}
	

}
