﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

public class DataReceive : MonoBehaviour {

	public GameObject _monkey;
	private Dictionary<int,GameObject> _bots;


	public GameObject _prefabplayer;
	private Dictionary<string,GameObject> _players; 

	public Terrain _terrain;
	// wörterbuch aller anderen spieler in dem wörterbuch kann man zum benutzernamen das gameobjekt auslesen

	public UnityEngine.UI.InputField txtChatVerlauf;

	// Use this for initialization
	void Start () {
		_players = new Dictionary<string,GameObject>();
		_bots = new Dictionary<int,GameObject>();
	}
	
	// Update is called once per frame
	void Update () { 

		//in str wird die nachricht gespeichert die vom server kommt
		string str = "";
		if (TcpConnection.inbuffer.Get (ref str)) //if nachricht vorhanden
		{
			string [] strarr = str.Split (new string []{"~"}, System.StringSplitOptions.RemoveEmptyEntries);// daten aufspaltung
			switch (strarr [0]) 
			{
			case "setposition": //damit verändert man die eigene position der kamera
				SetAtPos (strarr [1],this.gameObject);
				break;
			case "player": //zeichnet andere spieler an eine position
				DrawOtherPlayer(strarr);
				break;
			case "bot":
				DrawBot(strarr);
				break;
			case "c": // c steht für chat
				if(txtChatVerlauf.text == "")
					txtChatVerlauf.text = strarr[1];
				else
					txtChatVerlauf.text = txtChatVerlauf.text + "\n\r" + strarr[1];
				txtChatVerlauf.MoveTextEnd(false);
				break;
			}
		}

	}


	//damit verändert man die eigene position der kamera
	void SetAtPos(string pos,GameObject go)
	{
		string [] posarr = pos.Split(new string []{"*"},System.StringSplitOptions.RemoveEmptyEntries);
		go.transform.position = new Vector3 (float.Parse(posarr [0]), float.Parse(posarr [1]), float.Parse(posarr [2]));
		go.transform.rotation= new Quaternion (float.Parse(posarr [3]), float.Parse(posarr [4]), float.Parse(posarr [5]),float.Parse(posarr[6]));
	}

	//zeichnet andere spieler an eine position
	void DrawOtherPlayer(string [] arr)
	{
		string [] names = (string [])_players.Keys.ToArray<string>(); //usernamen aller vorhandenen spieler
		bool available = false;
		foreach (string s in names)
		{
			if(s == arr[1])
				available = true;
		}// prüfen ob gesendeter spieler schon existiert
		if (!available) // wenn nein wird hier spieler erstellt
		{
			GameObject go = (GameObject)Instantiate(_prefabplayer,Vector3.zero,new Quaternion(0,0,0,0));
			_players.Add (arr [1], go);
		}
		switch (arr [2]) //befehl für gesendeten spieler
		{
		case "position": // auf position setzen
			string [] posarr = arr[3].Split(new string []{"*"},System.StringSplitOptions.RemoveEmptyEntries);
			_players[arr[1]].transform.position = new Vector3 (float.Parse(posarr [0]), float.Parse(posarr [1]), float.Parse(posarr [2]));
			_players[arr[1]].transform.rotation = new Quaternion (float.Parse(posarr [3]), float.Parse(posarr [4]), float.Parse(posarr [5]),float.Parse(posarr[6]));
			break;
		}
	}

	void DrawBot(string [] arr)
	{
		int id = int.Parse (arr [1]);
		if(!_bots.ContainsKey(id))//Bot existiert noch nicht also muss einer erstellt werden
		{
			switch(arr[2])// typ des bots damit man weiß welcher typ erstellt werden muss
			{
			case "monkey"://_monkey ist der prefab für den monkey der gezeichnet wird und einmal im projekt existiert
				GameObject go = (GameObject)Instantiate(_monkey,Vector3.zero,new Quaternion(0,0,0,0));
				BotID botid = go.GetComponent<BotID>();
				botid.ID = id;
				_bots.Add(id,go);
				break;
			}
		}
		float x = float.Parse (arr [3]);
		float z = float.Parse (arr [4]);
		float y = _terrain.SampleHeight (new Vector3 (x, 0, z)) + 1f;
		_bots [id].transform.position = new Vector3 (x, y, z);
		_bots [id].transform.rotation = Quaternion.Euler(0, float.Parse(arr[5]), 0);

		int health = int.Parse(arr [6]);
		if (health < 0) 
		{
			//Bot stirbt
			Health hp = _bots [id].GetComponent<Health>();
			hp.Die();
			_bots.Remove(id);
		}
	}
	
}	
