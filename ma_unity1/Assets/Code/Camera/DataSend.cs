﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class DataSend : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {

		if(!TcpConnection.run)
			Application.LoadLevel(4);
		string str = "~position~" + transform.position.x + "*" + transform.position.y + "*" + transform.position.z + "*" + transform.rotation.x + "*" + transform.rotation.y + "*" + transform.rotation.z + "*" + transform.rotation.w + "~"; 
		TcpConnection.outbuffer.Put(ref str);
			
	
	}

	void OnDestroy()
	{
		string str = "~logout~";
		TcpConnection.outbuffer.Put (ref str);
		Thread.Sleep (200);
		TcpConnection.run = false;
	}


}
