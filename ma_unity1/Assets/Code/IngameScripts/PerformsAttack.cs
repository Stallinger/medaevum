using UnityEngine;
using System.Collections;

public class PerformsAttack : MonoBehaviour {
	
	public float range = 100.0f;
	public float cooldown = 0.2f;
	public GUIText dmgText;
	public GameObject arm;
	float hitCooldown = 0.0f;
	//public GameObject debrisPrefab;
	float cooldownRemaining = 0.0f;
	float dmgCooldownRemaining = 0.0f;
	public double damage = 2;
	bool hit = false;
	
	Vector3 xppos;
	
	// Use this for initialization
	void Start () {
		xppos.x = 0;
		xppos.y = 1;
		xppos.z = 0;

		Screen.showCursor = false;
	}
	
	// Update is called once per frame
	void Update () {
		//HandAusrichten();
		//DrawXP ();


		cooldownRemaining -= Time.deltaTime;
		dmgCooldownRemaining -= Time.deltaTime;
		hitCooldown -= Time.deltaTime;

		if ((hitCooldown <= 0) && hit) {
			arm.transform.Rotate (320, 0, 0);
			hit = false;
		}
		
		if ( Input.GetMouseButton (0) && cooldownRemaining <= 0){
			hitCooldown = 0.40f;
			hit = true;
			cooldownRemaining = cooldown;
			Ray ray = new Ray (Camera.main.transform.position, Camera.main.transform.forward);
			RaycastHit hitInfo;
			arm.transform.Rotate(40, 0, 0);
			
			if (Physics.Raycast(ray, out hitInfo, range)){
				Vector3 hitPoint = hitInfo.point;
				GameObject go = hitInfo.collider.gameObject;
				Debug.Log ("Hit Object: " + go.name);
				Debug.Log ("Hit Point: " + hitPoint);
				
				BotID botid = go.GetComponent<BotID>();
				
			
				int zahl = (int)Mathf.Round(Random.Range((float)(damage - (damage*0.15)), (float)(damage + (damage*0.15))));
				string sendText = "~bothit~" + botid.ID +"~" + zahl + "~";
				TcpConnection.outbuffer.Put(ref sendText);
			
				dmgText.text = "-" + zahl.ToString();
				dmgCooldownRemaining = (float)(cooldown * 0.8);

				
				/*if ( debrisPrefab != null) {
					if ( go.name != "Terrain"){
						for(int i = 0; i <= 4; i++) {
							Instantiate ( debrisPrefab, hitPoint, Quaternion.identity );
						}
					}
				}*/
			}
		}

		if(dmgCooldownRemaining <= 0) {
			dmgText.text = "";
		}
	}
}
